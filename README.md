```bash
npm ci

## Try out the current behavior
npm start
## You'll get this output:
# [registerAs] string
# [registerAs] undefined
# [onModuleInit] string
# [onModuleInit] number
# [onModuleInit] bar=2

## Then use the POC
npm run patch
npm start
## You'll get this output:
# [registerAs] string
# [registerAs] number
# [onModuleInit] string
# [onModuleInit] number
# [onModuleInit] bar=1
```
