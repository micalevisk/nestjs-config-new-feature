import { ConfigModule, registerAs, ConfigType, ConfigService } from '@nestjs/config'
import { Module, Inject } from '@nestjs/common'
import * as Joi from 'joi'

interface Environment {
  FOO: number
}

// @ts-ignore
const someConfig = registerAs('someConfig', (validatedEnvVars?: Environment) => {
  console.log('[registerAs]', typeof process.env.FOO) // string
  console.log('[registerAs]', typeof validatedEnvVars?.FOO) // number

  return {
    foo: process.env.FOO || 1, // *
  }
})

type SomeConfig = ConfigType<typeof someConfig>
/*
type SomeConfig = {
    foo: string | number;
}
*/

// @ts-ignore
const anotherConfig = registerAs('anotherConfig', (validatedEnvVars?: Environment) => {
  return {
    bar: validatedEnvVars?.FOO || 2,
  }
})
type AnotherConfig = ConfigType<typeof anotherConfig>
/*
type SomeConfig = {
    bar: number;
}
*/

@Module({
  imports: [
    // ConfigModule.forFeature(anotherConfig), // here it won't work

    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: false,
      load: [someConfig],
      validationSchema: Joi.object({
        FOO: Joi.number().default(1), // *
      }),
      validationOptions: {
        allowUnknown: true,
        stripUnknown: true,
        abortEarly: true,
      },
    }),

    ConfigModule.forFeature(anotherConfig), // here it will work

  ],
})
export class AppModule {
  constructor(
    private readonly configService: ConfigService,

    @Inject(someConfig.KEY)
    private readonly config: SomeConfig,
    @Inject(anotherConfig.KEY)
    private readonly anotherConfig: AnotherConfig,
  ) {}

  onModuleInit() {
    console.log('[onModuleInit]', typeof this.config.foo) // string
    console.log('[onModuleInit]', typeof this.configService.get('FOO')) // number
    console.log('[onModuleInit] bar=%d', this.anotherConfig.bar) // number
  }
}
